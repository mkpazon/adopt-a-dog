package com.mkpazon.adoptadog.api.pet;

/**
 * Created by mkpazon on 01/03/2018.
 */

public class Breeds {
    private Text[] breed;

    public void setBreed(Text[] breed) {
        this.breed = breed;
    }

    public Text[] getBreed() {
        return breed;
    }
}
