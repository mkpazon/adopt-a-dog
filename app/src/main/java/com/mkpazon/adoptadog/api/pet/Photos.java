package com.mkpazon.adoptadog.api.pet;

/**
 * Created by mkpazon on 04/03/2018.
 */

public class Photos {
    private Photo[] photo;

    public Photos() {
    }

    public Photos(Photo[] photo) {
        this.photo = photo;
    }

    public Photo[] getPhoto() {
        return photo;
    }

    public void setPhoto(Photo[] photo) {
        this.photo = photo;
    }
}
