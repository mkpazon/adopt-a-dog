package com.mkpazon.adoptadog.detail;

import com.mkpazon.adoptadog.api.pet.Pet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.Callable;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mkpazon on 04/03/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class DetailPresenterTest {

    @Mock
    private DetailView mView;

    @Mock
    private DetailInteractor mInteractor;

    @Mock
    private CompositeDisposable mDisposables;

    @Before
    public void setup() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @Test
    public void test_onActivityCreated_normal() throws Exception {
        Pet pet = new Pet();
        String photoUrl = "http://www.yehey.com";

        String petId = "123";
        DetailPresenter presenter = new DetailPresenter(mView, mInteractor, petId, mDisposables);
        Flowable<Pet> flowable = Flowable.just(pet);

        when(mInteractor.getPhotoUrl(pet)).thenReturn(photoUrl);
        when(mInteractor.getPetLocal(petId)).thenReturn(flowable);

        presenter.onActivityCreated();

        verify(mView, times(1)).displayPetDetails(pet, photoUrl);
    }

    @Test
    public void test_onActivityCreated_error() throws Exception {
        Pet pet = new Pet();
        String photoUrl = "http://www.yehey.com";

        String petId = "123";
        DetailPresenter presenter = new DetailPresenter(mView, mInteractor, petId, mDisposables);
        Flowable<Pet> flowable = Flowable.just(pet)
                .map(new Function<Pet, Pet>() {
                    @Override
                    public Pet apply(Pet pet) throws Exception {
                        throw new RuntimeException("Test exception");
                    }
                });

        when(mInteractor.getPetLocal(petId)).thenReturn(flowable);

        presenter.onActivityCreated();

        verify(mView, never()).displayPetDetails(pet, photoUrl);
    }

    @Test
    public void cleanup() throws Exception {
        DetailPresenter presenter = new DetailPresenter(mView, mInteractor, "123", mDisposables);
        presenter.cleanup();
        verify(mDisposables, times(1)).clear();
    }
}