package com.mkpazon.adoptadog.detail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mkpazon.adoptadog.R;
import com.mkpazon.adoptadog.api.pet.Pet;
import com.mkpazon.adoptadog.api.pet.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailFragment extends Fragment implements DetailView {

    public static final String ARG_PET_ID = "petId";

    private DetailPresenter mPresenter;
    private OnDetailFragmentListener mListener;
    private Unbinder mUnbinder;

    @BindView(R.id.textView_gender)
    TextView mTvGender;

    @BindView(R.id.textView_age)
    TextView mTvAge;

    @BindView(R.id.textView_description)
    TextView mTvDescription;

    @BindView(R.id.textView_breed)
    TextView mTvBreed;

    public DetailFragment() {
    }

    public static DetailFragment newInstance(String petId) {
        DetailFragment detailFragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_PET_ID, petId);
        detailFragment.setArguments(bundle);
        return detailFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDetailFragmentListener) {
            mListener = (OnDetailFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnDetailFragmentListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle data = getArguments();
        String petId = data.getString(ARG_PET_ID);
        mPresenter = new DetailPresenter(this, new DetailInteractor(getActivity()), petId, new CompositeDisposable());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onActivityCreated();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.cleanup();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void displayPetDetails(Pet pet, String photoUrl) {
        mListener.onPetInfoUpdated(pet, photoUrl);
        mTvGender.setText(pet.getSex().getValue());
        mTvAge.setText(pet.getAge().getValue());
        mTvDescription.setText(pet.getDescription().getValue());

        Text[] txtBreeds = pet.getBreeds().getBreed();
        StringBuilder strBuilder = new StringBuilder();
        for (Text text : txtBreeds) {
            if (strBuilder.length() > 0) {
                strBuilder.append(", ");
            }
            strBuilder.append(text.getValue());
        }
        mTvBreed.setText(strBuilder.toString());
    }

    interface OnDetailFragmentListener {
        void onPetInfoUpdated(Pet pet, String photoUrl);
    }
}
