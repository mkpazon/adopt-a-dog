package com.mkpazon.adoptadog.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.facebook.drawee.view.SimpleDraweeView;
import com.mkpazon.adoptadog.R;
import com.mkpazon.adoptadog.api.pet.Pet;
import com.mkpazon.adoptadog.list.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class DetailActivity extends AppCompatActivity implements DetailFragment.OnDetailFragmentListener {

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout mCollapsingToolbarLayout;

    @BindView(R.id.imageView_backdrop)
    SimpleDraweeView draweeView;


    public static final String EXTRA_PET_ID = "petId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            String petId = getIntent().getStringExtra(DetailFragment.ARG_PET_ID);
            Fragment fragment = DetailFragment.newInstance(petId);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, MainActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPetInfoUpdated(Pet pet, String photoUrl) {
        Timber.d(".onPetInfoUpdated");
        mCollapsingToolbarLayout.setTitle(pet.getName().getValue());
        if (photoUrl != null) {
            Uri uri = Uri.parse(photoUrl);
            draweeView.setImageURI(uri);
        }
    }
}
