package com.mkpazon.adoptadog.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mkpazon.adoptadog.R;
import com.mkpazon.adoptadog.api.pet.Pet;
import com.mkpazon.adoptadog.detail.DetailActivity;
import com.mkpazon.adoptadog.ui.adapter.OnListInteractionListener;
import com.mkpazon.adoptadog.ui.adapter.PetfinderAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class MainFragment extends Fragment implements MainView, OnListInteractionListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.list)
    RecyclerView mRvDogs;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private MainPresenter mPresenter;
    private Unbinder mUnbinder;
    private PetfinderAdapter mAdapter;

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Timber.d(".onCreateView");
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        Context context = view.getContext();
        mRvDogs.setLayoutManager(new LinearLayoutManager(context));
        mSwipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        Timber.d(".onDestroyView");
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onClickItem(Pet item) {
        mPresenter.onClickItem(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Timber.d(".onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        List<Pet> pets;
        if (mPresenter == null) {
            pets = new ArrayList<>();
            mPresenter = new MainPresenter(this, new MainInteractor(getActivity()), pets, new CompositeDisposable());
        } else {
            pets = mPresenter.getPets();
        }
        mAdapter = new PetfinderAdapter(pets, this);
        mRvDogs.setAdapter(mAdapter);
        mPresenter.onInitializationDone();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Timber.d(".onCreate");
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDestroy() {
        Timber.d(".onDestroy");
        super.onDestroy();
        mPresenter.cleanUp();
    }

    @Override
    public void onRefresh() {
        Timber.d(".onRefresh");
        mPresenter.onRefresh();
    }

    @Override
    public void updateListView() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setListLoading(boolean isLoading) {
        mSwipeRefreshLayout.setRefreshing(isLoading);
    }

    @Override
    public void launchDetailScreen(Pet pet) {
        Timber.d(".launchDetailScreen");
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(DetailActivity.EXTRA_PET_ID, pet.getId().getValue());
        startActivity(intent);
    }

    public static Fragment newInstance() {
        return new MainFragment();
    }
}
