package com.mkpazon.adoptadog.list;

import android.app.Application;
import android.content.Context;

import com.mkpazon.adoptadog.api.WebServiceProvider;
import com.mkpazon.adoptadog.api.pet.GetPetsResponse;
import com.mkpazon.adoptadog.api.pet.Pet;
import com.mkpazon.adoptadog.dao.PetDatabase;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by mkpazon on 27/02/2018.
 */

class MainInteractor {
    private final Context mContext;

    MainInteractor(Context context) {
        if (!(context instanceof Application)) {
            mContext = context.getApplicationContext();
        } else {
            mContext = context;
        }
    }

    Observable<Response<GetPetsResponse>> getPetsServer() {
        return WebServiceProvider.getWebService(mContext).getPets();
    }

    Flowable<Pet[]> getPetsLocal() {
        return PetDatabase.getAppDatabase(mContext).petDao().getPets();
    }

    void insertLocal(Pet[] pets) {
        PetDatabase.getAppDatabase(mContext).petDao().insert(pets);
    }
}
