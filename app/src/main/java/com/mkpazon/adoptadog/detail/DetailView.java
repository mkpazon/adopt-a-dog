package com.mkpazon.adoptadog.detail;

import com.mkpazon.adoptadog.api.pet.Pet;

/**
 * Created by mkpazon on 02/03/2018.
 */

interface DetailView {
    void displayPetDetails(Pet pet, String photoUrl);
}
