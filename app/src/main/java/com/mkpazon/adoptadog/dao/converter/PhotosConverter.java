package com.mkpazon.adoptadog.dao.converter;

import android.arch.persistence.room.TypeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mkpazon.adoptadog.api.pet.Photo;
import com.mkpazon.adoptadog.api.pet.Photos;

import java.io.IOException;

/**
 * Created by mkpazon on 03/03/2018.
 */

public class PhotosConverter {
    private static ObjectMapper mObjectMapper;

    @TypeConverter
    public static String fromPhotos(Photos photos) {
        Photo[] photoArr =  photos.getPhoto();
        if (mObjectMapper == null) {
            mObjectMapper = new ObjectMapper();
        }

        try {
            return mObjectMapper.writeValueAsString(photoArr);
        } catch (JsonProcessingException e) {
            return "";
        }
    }


    @TypeConverter
    public static Photos toPhotos(String json)  {
        if (mObjectMapper == null) {
            mObjectMapper = new ObjectMapper();
        }
        try {
            Photo[] photoArr = mObjectMapper.readValue(json, Photo[].class);
            return new Photos(photoArr);
        } catch (IOException e) {
            return new Photos(new Photo[0]);
        }
    }
}
