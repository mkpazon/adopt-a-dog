package com.mkpazon.adoptadog.list;

import com.mkpazon.adoptadog.api.pet.Pet;

interface MainView {

    void updateListView();

    void setListLoading(boolean isLoading);

    void launchDetailScreen(Pet pet);
}
