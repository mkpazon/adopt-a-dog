package com.mkpazon.adoptadog.ui.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.mkpazon.adoptadog.R;
import com.mkpazon.adoptadog.api.pet.Breeds;
import com.mkpazon.adoptadog.api.pet.Pet;
import com.mkpazon.adoptadog.api.pet.Text;
import com.mkpazon.adoptadog.util.PhotoRetriever;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PetfinderAdapter extends RecyclerView.Adapter<PetfinderAdapter.ViewHolder> {

    private List<Pet> mItems;
    private OnListInteractionListener mListener;

    public PetfinderAdapter(List<Pet> items, OnListInteractionListener listener) {
        mListener = listener;
        mItems = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Pet item = mItems.get(position);
        holder.mTextView1.setText(item.getName().getValue());
        holder.mTextView2.setText(getBreedsText(item.getBreeds()));
        holder.mTextView3.setText(item.getAge().getValue());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onClickItem(item);
                }
            }
        });
        String photoUrl = PhotoRetriever.getFirstPhotoUrlThumbnail(item);
        if (photoUrl != null) {
            Uri uri = Uri.parse(photoUrl);
            holder.mIvPhoto.setImageURI(uri);
        }
    }

    private String getBreedsText(Breeds breeds) {
        Text[] txtBreeds = breeds.getBreed();
        StringBuilder strBuilder = new StringBuilder();
        for (Text text : txtBreeds) {
            if (strBuilder.length() > 0) {
                strBuilder.append(", ");
            }
            strBuilder.append(text.getValue());
        }
        return strBuilder.toString();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView1)
        TextView mTextView1;

        @BindView(R.id.textView2)
        TextView mTextView2;

        @BindView(R.id.textView3)
        TextView mTextView3;

        @BindView(R.id.imageView_photo)
        SimpleDraweeView mIvPhoto;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextView2.getText() + "'";
        }
    }
}
