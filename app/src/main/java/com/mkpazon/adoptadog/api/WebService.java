package com.mkpazon.adoptadog.api;

import com.mkpazon.adoptadog.api.pet.GetPetsResponse;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created by mkpazon on 27/02/2018.
 */

public interface WebService {

    // IMPORTANT: Hardcoded location to zipcode 10001 so that API may return some results
    @GET("pet.find?format=json&animal=dog&location=10001")
    Observable<Response<GetPetsResponse>> getPets();




}
