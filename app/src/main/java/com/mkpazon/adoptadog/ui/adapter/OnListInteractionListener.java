package com.mkpazon.adoptadog.ui.adapter;

import com.mkpazon.adoptadog.api.pet.Pet;

public interface OnListInteractionListener {
    void onClickItem(Pet pet);
}
