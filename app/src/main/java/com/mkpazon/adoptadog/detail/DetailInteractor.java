package com.mkpazon.adoptadog.detail;

import android.app.Application;
import android.content.Context;

import com.mkpazon.adoptadog.api.pet.Pet;
import com.mkpazon.adoptadog.dao.PetDatabase;
import com.mkpazon.adoptadog.util.PhotoRetriever;

import io.reactivex.Flowable;

/**
 * Created by mkpazon on 04/03/2018.
 */

class DetailInteractor {
    private final Context mContext;

    DetailInteractor(Context context) {
        if (!(context instanceof Application)) {
            context = context.getApplicationContext();
        }
        mContext = context;
    }

    Flowable<Pet> getPetLocal(String petId) {
        return PetDatabase.getAppDatabase(mContext).petDao().getPet(petId);
    }

    String getPhotoUrl(Pet pet) {
        return PhotoRetriever.getFirstPhotoUrl(pet);
    }
}
