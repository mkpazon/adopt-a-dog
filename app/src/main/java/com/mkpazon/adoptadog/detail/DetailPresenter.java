package com.mkpazon.adoptadog.detail;

import com.mkpazon.adoptadog.api.pet.Pet;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.ResourceSubscriber;
import timber.log.Timber;

/**
 * Created by mkpazon on 02/03/2018.
 */

class DetailPresenter {
    private final DetailView mView;
    private final String mPetId;
    private final DetailInteractor mInteractor;
    private CompositeDisposable mDisposables;

    DetailPresenter(DetailView view, DetailInteractor interactor, String petId, CompositeDisposable disposables) {
        mInteractor = interactor;
        mView = view;
        mPetId = petId;
        mDisposables = disposables;
    }


    void onActivityCreated() {
        mDisposables.add(mInteractor.getPetLocal(mPetId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ResourceSubscriber<Pet>() {
                    @Override
                    public void onNext(Pet pet) {
                        Timber.d("[getPet]->.onActivityCreated");

                        final String photoUrl = mInteractor.getPhotoUrl(pet);
                        mView.displayPetDetails(pet, photoUrl);
                    }

                    @Override
                    public void onError(Throwable t) {
                        Timber.e(t, "[getPet]->.onError");
                    }

                    @Override
                    public void onComplete() {
                        Timber.d(".onComplete");
                    }
                }));
    }

    void cleanup() {
        mDisposables.clear();
    }


}
