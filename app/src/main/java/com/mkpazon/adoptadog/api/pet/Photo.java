package com.mkpazon.adoptadog.api.pet;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mkpazon on 04/03/2018.
 */

public class Photo {

    @JsonProperty("@size")
    private String size;

    @JsonProperty("$t")
    private String url;

    @JsonProperty("@id")
    private String id;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
