package com.mkpazon.adoptadog.api.pet;

/**
 * Created by mkpazon on 01/03/2018.
 */

public class Contact {
    private Text phone;
    private Text state;
    private Text city;
    private Text zip;
    private Text email;

    public Contact() {

    }

    public Text getPhone() {
        return phone;
    }

    public Text getState() {
        return state;
    }

    public Text getCity() {
        return city;
    }

    public Text getZip() {
        return zip;
    }

    public Text getEmail() {
        return email;
    }

    public void setPhone(Text phone) {
        this.phone = phone;
    }

    public void setState(Text state) {
        this.state = state;
    }

    public void setCity(Text city) {
        this.city = city;
    }

    public void setZip(Text zip) {
        this.zip = zip;
    }

    public void setEmail(Text email) {
        this.email = email;
    }
}
