package com.mkpazon.adoptadog.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.mkpazon.adoptadog.api.pet.Pet;
import com.mkpazon.adoptadog.dao.converter.PhotosConverter;
import com.mkpazon.adoptadog.dao.converter.TextConverter;

/**
 * Created by mkpazon on 03/03/2018.
 */

@Database(version = 1, entities = {Pet.class})
@TypeConverters({TextConverter.class, PhotosConverter.class})
public abstract class PetDatabase extends RoomDatabase {
    private static PetDatabase INSTANCE;

    abstract public PetDao petDao();

    public static PetDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), PetDatabase.class, "pet-database")
                    .build();
        }
        return INSTANCE;
    }

}
