package com.mkpazon.adoptadog.api;

import android.content.Context;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.mkpazon.adoptadog.App;
import com.mkpazon.adoptadog.Configuration;
import com.mkpazon.adoptadog.Constants;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by mkpazon on 27/02/2018.
 */

public class WebServiceProvider {
    private static WebService mWebService;
    private static OkHttpClient mOkHttpClient;
    private static ObjectMapper mObjectMapper = new ObjectMapper();

    private static void init(Context context) {
        if (!(context instanceof App)) {
            context = context.getApplicationContext();
        }

        initOkhttpClient(context);

        Retrofit mRetrofit = new Retrofit.Builder()
                .client(mOkHttpClient)
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create(getDefaultObjectMapper()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        mWebService = mRetrofit.create(WebService.class);
    }

    private static void initOkhttpClient(Context context) {
        if (mOkHttpClient == null) {
            // Setup cookie store
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder().cookieJar(new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context)));
            Configuration.configApiHandler(httpClientBuilder);

            // Add api key to all requests
            httpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    HttpUrl originalHttpUrl = original.url();

                    HttpUrl url = originalHttpUrl.newBuilder()
                            .addQueryParameter("key", Constants.API_KEY)
                            .build();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .url(url);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            mOkHttpClient = httpClientBuilder.build();
        }
    }

    public static ObjectMapper getDefaultObjectMapper() {
        return mObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                .setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CAMEL_CASE);
    }

    public static WebService getWebService(Context context) {
        if (mWebService == null) {
            init(context);
        }
        return mWebService;
    }
}
