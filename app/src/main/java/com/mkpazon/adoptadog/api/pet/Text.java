package com.mkpazon.adoptadog.api.pet;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mkpazon on 01/03/2018.
 */

public class Text implements Parcelable {
    private String value;

    public Text() {

    }

    public void setValue(String value) {
        this.value = value;
    }

    public Text(String value) {
        this.value = value;
    }

    protected Text(Parcel in) {
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Text> CREATOR = new Creator<Text>() {
        @Override
        public Text createFromParcel(Parcel in) {
            return new Text(in);
        }

        @Override
        public Text[] newArray(int size) {
            return new Text[size];
        }
    };

    @JsonProperty("$t")
    public String getValue() {
        return value;
    }
}
