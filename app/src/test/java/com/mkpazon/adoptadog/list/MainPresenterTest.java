package com.mkpazon.adoptadog.list;

import com.mkpazon.adoptadog.api.pet.GetPetsResponse;
import com.mkpazon.adoptadog.api.pet.Pet;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mkpazon on 04/03/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    @Mock
    private MainView mView;

    @Mock
    private MainInteractor mInteractor;

    @Mock
    private CompositeDisposable mDisposables;

    @Before
    public void setUp() throws Exception {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @Test
    public void test_onClickItem() throws Exception {
        Pet pet = new Pet();
        MainPresenter presenter = new MainPresenter(mView, mInteractor, ArgumentMatchers.<Pet>anyList(), mDisposables);
        presenter.onClickItem(pet);
        verify(mView, times(1)).launchDetailScreen(pet);
    }

    @Test
    public void test_onClickItem_null() throws Exception {
        MainPresenter presenter = new MainPresenter(mView, mInteractor, new ArrayList<Pet>(), mDisposables);
        presenter.onClickItem(null);
        verify(mView, never()).launchDetailScreen(null);
    }


    @Test
    public void onInitializationDone() throws Exception {
    }

    @Test
    public void test_onRefresh() throws Exception {
        List<Pet> pets = new ArrayList<>();
        pets.add(new Pet());
        pets.add(new Pet());
        pets.add(new Pet());

        Flowable<Pet[]> flowable = Flowable.just(pets.toArray(new Pet[pets.size()]));
        when(mInteractor.getPetsLocal()).thenReturn(flowable);

        Response<GetPetsResponse> response = mock(Response.class);

        Observable<Response<GetPetsResponse>> observable = Observable.just(response);
        when(mInteractor.getPetsServer()).thenReturn(observable);

        MainPresenter presenter = new MainPresenter(mView, mInteractor, pets, mDisposables);
        presenter.onRefresh();

        verify(mInteractor).getPetsLocal();
        verify(mInteractor).getPetsServer();
    }


    @Test
    public void test_onInitializationDone_empty() throws Exception {
        List<Pet> pets = new ArrayList<>();

        Flowable<Pet[]> flowable = Flowable.just(pets.toArray(new Pet[pets.size()]));
        when(mInteractor.getPetsLocal()).thenReturn(flowable);

        Response<GetPetsResponse> response = mock(Response.class);

        Observable<Response<GetPetsResponse>> observable = Observable.just(response);
        when(mInteractor.getPetsServer()).thenReturn(observable);

        MainPresenter presenter = new MainPresenter(mView, mInteractor, pets, mDisposables);
        presenter.onInitializationDone();

        verify(mInteractor).getPetsLocal();
        verify(mInteractor).getPetsServer();
    }

    @Test
    public void test_onInitializationDone_not_empty() throws Exception {
        List<Pet> pets = new ArrayList<>();
        pets.add(new Pet());
        pets.add(new Pet());
        pets.add(new Pet());

        MainPresenter presenter = new MainPresenter(mView, mInteractor, pets, mDisposables);
        presenter.onInitializationDone();

        verify(mInteractor,never()).getPetsLocal();
        verify(mInteractor,never()).getPetsServer();
    }

    @Test
    public void cleanUp() throws Exception {
        MainPresenter presenter = new MainPresenter(mView, mInteractor, new ArrayList<Pet>(), mDisposables);
        presenter.cleanUp();
        verify(mDisposables, times(1)).clear();
    }

    @Test
    public void getPets() throws Exception {
        List<Pet> pets = new ArrayList<>();
        MainPresenter presenter = new MainPresenter(mView, mInteractor, pets, mDisposables);
        Assert.assertEquals(pets, presenter.getPets());
    }

}