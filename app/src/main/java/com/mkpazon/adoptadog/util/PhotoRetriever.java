package com.mkpazon.adoptadog.util;

import com.mkpazon.adoptadog.api.pet.Pet;
import com.mkpazon.adoptadog.api.pet.Photo;

/**
 * Created by mkpazon on 04/03/2018.
 */

public class PhotoRetriever {
    public static String getFirstPhotoUrl(Pet pet) {
        try {
            Photo[] photos = pet.getMedia().getPhotos().getPhoto();
            for(Photo photo : photos) {
                if(photo.getId().equals("1") && photo.getSize().equals("x")) {
                    return photo.getUrl();
                }
            }
        } catch (Exception e) {
            // Ignore all exceptions related to retrieval of url
        }
        return null;
    }

    public static String getFirstPhotoUrlThumbnail(Pet pet) {
        try {
            Photo[] photos = pet.getMedia().getPhotos().getPhoto();
            for(Photo photo : photos) {
                if(photo.getId().equals("1") && photo.getSize().equals("t")) {
                    return photo.getUrl();
                }
            }
        } catch (Exception e) {
            // Ignore all exceptions related to retrieval of url
        }
        return null;
    }
}
