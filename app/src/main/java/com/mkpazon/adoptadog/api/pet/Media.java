package com.mkpazon.adoptadog.api.pet;

/**
 * Created by mkpazon on 04/03/2018.
 */

public class Media {
    private Photos photos;

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }
}
