package com.mkpazon.adoptadog.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.mkpazon.adoptadog.api.pet.Pet;

import io.reactivex.Flowable;

/**
 * Created by mkpazon on 03/03/2018.
 */

@Dao
public interface PetDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Pet[] pets);

    @Query("SELECT * from pet ORDER BY id")
    Flowable<Pet[]> getPets();

    @Query("SELECT * from pet where id=:petId")
    Flowable<Pet> getPet(String petId);
}
