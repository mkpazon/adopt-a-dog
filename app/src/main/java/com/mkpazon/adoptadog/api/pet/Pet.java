package com.mkpazon.adoptadog.api.pet;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by mkpazon on 01/03/2018.
 */


@Entity
public class Pet {
    private Text age;
    private Text animal;
    private Text description;
    private Text name;
    private Text sex;


    @PrimaryKey
    @NonNull
    private Text id;

    @Embedded(prefix = "breed_")
    private Breeds breeds;

    @Embedded(prefix = "contact_")
    private Contact contact;

    @Embedded(prefix = "media_")
    private Media media;

    public Breeds getBreeds() {
        return breeds;
    }

    private Text size;
    private Text shelterId;

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public Text getAge() {
        return age;
    }

    public Text getAnimal() {
        return animal;
    }

    public Contact getContact() {
        return contact;
    }

    public Text getDescription() {
        return description;
    }

    public Text getId() {
        return id;
    }

    public Text getName() {
        return name;
    }

    public Text getSex() {
        return sex;
    }

    public Text getSize() {
        return size;
    }

    public Text getShelterId() {
        return shelterId;
    }


    public void setAge(Text age) {
        this.age = age;
    }

    public void setAnimal(Text animal) {
        this.animal = animal;
    }

    public void setDescription(Text description) {
        this.description = description;
    }

    public void setName(Text name) {
        this.name = name;
    }

    public void setSex(Text sex) {
        this.sex = sex;
    }

    public void setId(Text id) {
        this.id = id;
    }

    public void setBreeds(Breeds breeds) {
        this.breeds = breeds;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public void setSize(Text size) {
        this.size = size;
    }

    public void setShelterId(Text shelterId) {
        this.shelterId = shelterId;
    }
}
