package com.mkpazon.adoptadog.list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mkpazon.adoptadog.R;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.d(".onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_main_container, MainFragment.newInstance())
                    .commit();
        }
    }
}
