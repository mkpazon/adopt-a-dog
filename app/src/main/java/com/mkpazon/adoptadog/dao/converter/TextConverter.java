package com.mkpazon.adoptadog.dao.converter;

import android.arch.persistence.room.TypeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mkpazon.adoptadog.api.pet.Text;

import java.io.IOException;

/**
 * Created by mkpazon on 03/03/2018.
 */

public class TextConverter {
    private static ObjectMapper mObjectMapper;

    @TypeConverter
    public static String fromText(Text text) {
        return text.getValue();
    }

    @TypeConverter
    public static Text toText(String str) {
        return new Text(str);
    }

    @TypeConverter
    public static String fromTextArray(Text[] arr)  {
        if (mObjectMapper == null) {
            mObjectMapper = new ObjectMapper();
        }
        try {
            return mObjectMapper.writeValueAsString(arr);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    @TypeConverter
    public static Text[] toTextArray(String jsonArr)  {
        if (mObjectMapper == null) {
            mObjectMapper = new ObjectMapper();
        }
        try {
            return mObjectMapper.readValue(jsonArr, Text[].class);
        } catch (IOException e) {
            return new Text[0];
        }
    }
}
