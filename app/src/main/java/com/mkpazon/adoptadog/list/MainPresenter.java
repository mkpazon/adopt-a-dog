package com.mkpazon.adoptadog.list;

import com.mkpazon.adoptadog.api.pet.GetPetsResponse;
import com.mkpazon.adoptadog.api.pet.Pet;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.ResourceSubscriber;
import retrofit2.Response;
import timber.log.Timber;

class MainPresenter {
    private final MainInteractor mInteractor;
    private CompositeDisposable mDisposables;
    private MainView mView;
    private List<Pet> mPets;

    MainPresenter(MainView view, MainInteractor interactor, List<Pet> pets, CompositeDisposable disposables) {
        mView = view;
        mInteractor = interactor;
        mPets = pets;
        mDisposables = disposables;
    }

    void onClickItem(Pet pet) {
        if (pet != null) {
            mView.launchDetailScreen(pet);
        }
    }

    void onInitializationDone() {
        Timber.d(".onInitializationDone");
        reloadData();
    }

    private void updateListview(Pet[] pets) {
        mPets.addAll(Arrays.asList(pets));
        mView.updateListView();
    }

    private Observable<Response<GetPetsResponse>> retrievePetsAsync() {
        return mInteractor.getPetsServer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    void onRefresh() {
        Timber.d(".onRefresh");
        mPets.clear();
        reloadData();
    }

    private void reloadData() {
        if (mPets.size() != 0) {
            mView.updateListView();
        } else {
            getPetsFromDB();
            retrievePetFromServer();
        }
    }

    private void getPetsFromDB() {
        mDisposables.add(mInteractor.getPetsLocal()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ResourceSubscriber<Pet[]>() {
                    @Override
                    public void onNext(Pet[] pets) {
                        Timber.d("[getPetsFromDB]->.onNext");
                        updateListview(pets);
                    }

                    @Override
                    public void onError(Throwable t) {
                        Timber.e(t, ".onError");
                    }

                    @Override
                    public void onComplete() {
                        Timber.d(".onComplete");
                    }
                }));
    }

    private void retrievePetFromServer() {
        // TODO move to a service?
        retrievePetsAsync()
                .doOnTerminate(new Action() {
                    @Override
                    public void run() throws Exception {
                        mView.setListLoading(false);
                    }
                })
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        mView.setListLoading(true);
                    }
                })
                .subscribeWith(new DisposableObserver<Response<GetPetsResponse>>() {
                    @Override
                    public void onNext(Response<GetPetsResponse> response) {
                        Timber.d("->.onNext");
                        // TODO handle error case
                        GetPetsResponse body = response.body();

                        Pet[] pets = body.getPetfinder().getPets().getPet();
                        cache(pets);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "->.onError");
                    }

                    @Override
                    public void onComplete() {
                        Timber.d("->.onComplete");

                    }
                });
    }

    private void cache(final Pet[] pets) {
        Timber.d(".cache");

        mDisposables.add(Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter e) throws Exception {
                mInteractor.insertLocal(pets);
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Timber.d("->.onComplete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "->.onError");
                    }
                }));
    }

    void cleanUp() {
        mDisposables.clear();
    }

    List<Pet> getPets() {
        return mPets;
    }
}
